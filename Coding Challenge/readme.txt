
Coding Challenge
Setup instructions:

Development Environment Setup:

Pre-requisites:

NodeJS

To install node modules

$ npm install

To run  application:

$ node server.js

To run  test files:

$ node completeTest.js
$ node singleTest.js

To call rest api:

curl -H "Content-Type: application/json" -X POST -d "{\"input\":[[5,4,5],[1,1],[2,2,4],[2,3,5],[2,2,4],[1,5],[1,2,15],[1,3,5],[1,5,5],[5,4,10]]}" http://localhost:3000/api/shopping/synchronous

Post data:

send an array as each line of the input
example

{"input":[
	[5, 4, 5],
	[1, 1],
	[2, 2, 4],
	[2, 3, 5],
	[2, 2, 4],
	[1, 5],
	[1, 2, 15],
	[1, 3, 5],
	[1, 5, 5],
	[5, 4, 10]
	]
}

