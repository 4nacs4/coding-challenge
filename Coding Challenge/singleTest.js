var express = require("express");
var app = express();
var config = require('./app/config');
var SynchronousShopping = require('./app/controllers/synchronousShopping');
var Utils = require('./app/services/utils');
var ShoppingRoute = require('./app/models/ShoppingRoute');
var input1 = require('./tests/input1.json');
var input2 = require('./tests/input2.json');
var input3 = require('./tests/input3.json');
var input4 = require('./tests/input4.json');
var input5 = require('./tests/input5.json');
var input6 = require('./tests/input6.json');
var input7 = require('./tests/input7.json');
var input8 = require('./tests/input8.json');


app.listen(config.PORT, function() {
    console.log("Node test server running on http://localhost:3000");
    
    console.log("----------TESTING INPUT DATA----------")
    if((SynchronousShopping.checkInputData(input1).valid)){
         console.log("PASSED");
    }
    else{
        console.log("ERROR");
    }
    console.log("-------------------------------------")
    
    console.log("----------TESTING BUILD DATA----------")
    var shoppingCenterData = (SynchronousShopping.buildInputData(input2.input, input2.totalNodes, input2.totalConnections));
    if(shoppingCenterData.length == input2.totalNodes){
         console.log("PASSED");
    }
    else{
        console.log("ERROR");
    }
    console.log("-------------------------------------")
    
    console.log("----------TESTING FULL CONNECTIONS BETWEEN NODES----------")
    if((SynchronousShopping.checkAllShoppingCenterRoutes([1], 1, shoppingCenterData)).length == input3.totalNodes){
         console.log("PASSED");
    }
    else{
        console.log("ERROR");
    }
    console.log("-------------------------------------")
    
    console.log("----------TESTING SHORTEST GRAPH----------")
    if(Utils.buildDijkstraGraph(shoppingCenterData).graph){
         console.log("PASSED");
    }
    else{
        console.log("ERROR");
    }
    console.log("-------------------------------------")
    
    console.log("----------TESTING POSIBLE ROUTES----------")
    var firstRoute =  new ShoppingRoute([1], [1]);
    var routesList = SynchronousShopping.findShoppingCenterRoutes([firstRoute], shoppingCenterData);
    if(routesList.length > 0){
         console.log("PASSED");
    }
    else{
        console.log("ERROR");
    }
    
    console.log("-------------------------------------")
    console.log("----------TESTING BEST TWO ROUTES----------")
    if(SynchronousShopping.chooseBestTwoRoutes(routesList, input5.totalFishes, input5.maxTime).maxTime){
         console.log("PASSED");
    }
    else{
        console.log("ERROR");
    }
    console.log("-------------------------------------")
});


