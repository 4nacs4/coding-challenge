var express = require("express");
var router = express.Router();
var shopping = require('../controllers/shopping');


//Shopping Routes
router.post('/api/shopping/synchronous', shopping.startSynchronous);

module.exports = router;
