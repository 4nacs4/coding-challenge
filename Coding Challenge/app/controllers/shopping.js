var SynchronousShopping = require('./synchronousShopping');
var ShoppingRoute = require('../models/ShoppingRoute');
var _ = require('underscore');
var Config = require('../Config');
var Utils = require('../services/utils');

var shopping = {
    startSynchronous: function(req, res) {
        var shoppingCenterList = [],
                shortRoutesBetweenShoppingCenters = [],
                newShoppingRoute = {},
                shoppingRouteList = [],
                newShoppingRouteList = [],
                bestTwoRoutes = {},
                totalShoppingCenters = 0,
                totalRoads = 0,
                totalFishes = 0,
                inputDataChecked = [],
                inputData = [],
                configInitParams = [],
                visitedShoppingCenters = [],
                maxTime = 0;
        
        //check inputData format and data
        inputDataChecked = SynchronousShopping.checkInputData(req.body);
        if (!inputDataChecked.valid) {
            return res.status(400).send({
                type: "ERROR",
                reason: inputDataChecked.reason
            });
        }
        //Init params
        inputData = req.body.input;
        configInitParams = inputData[0];
        totalShoppingCenters = configInitParams[0];
        totalRoads = configInitParams[1];
        totalFishes = configInitParams[2];
        //Build list of shopping center with their respective roads
        shoppingCenterList = SynchronousShopping.buildInputData(inputData, totalShoppingCenters, totalRoads);
        if(shoppingCenterList.length != totalShoppingCenters){
            return res.status(400).send({
                type: "ERROR",
                reason: Config.ERROR_MSG.TYPE_11
            });
        }
        //check if is posible to get to any shopping center from any other shopping center
        visitedShoppingCenters = SynchronousShopping.checkAllShoppingCenterRoutes([1], 1, shoppingCenterList);
        if(visitedShoppingCenters.length < shoppingCenterList.length){
            return res.status(400).send({
                type: "ERROR",
                reason: Config.ERROR_MSG.TYPE_10
            });
        }
        //build a graph with the shortests routes between shopping centers
        shortRoutesBetweenShoppingCenters = Utils.buildDijkstraGraph(shoppingCenterList);
        if(shortRoutesBetweenShoppingCenters.length == 0){
            return res.status(400).send({
                type: "ERROR",
                reason: Config.ERROR_MSG.TYPE_12
            });
        }
        //Instance the start route with its respective fish
        newShoppingRoute = new ShoppingRoute([1], shoppingCenterList[0].getFishes());
        //create a list of all posible routes where all fish can be collected
        newShoppingRouteList = SynchronousShopping.findShoppingCenterRoutes([newShoppingRoute], shoppingCenterList);
        shoppingRouteList = _.union([newShoppingRoute], newShoppingRouteList); 
        //Calculate total time by route and get the max time of all routes
        _.each(shoppingRouteList, function(shoppingRoute) {
            //Add the end of the route
            shoppingRoute.addLastShoppingCenterToBeVisited(shoppingCenterList);
            shoppingRoute.getTotalTimeRoute(shortRoutesBetweenShoppingCenters);
            if(shoppingRoute.totalTime > maxTime)
                maxTime = shoppingRoute.totalTime;
        });
        //Choose the two best routes 
        bestTwoRoutes = SynchronousShopping.chooseBestTwoRoutes(shoppingRouteList, totalFishes, maxTime);
        if(!bestTwoRoutes.maxTime){
            return res.status(400).send({
                type: "ERROR",
                reason: Config.ERROR_MSG.TYPE_13
            });
        }
        //finish
        return res.status(400).send({
            type: "OK",
            output:{bestTime: bestTwoRoutes}
        });
    }
};
module.exports = shopping;