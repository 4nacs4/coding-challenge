var ShoppingCenter = require('../models/ShoppingCenter');
var ShoppingRoute = require('../models/ShoppingRoute');
var _ = require('underscore');
var Config = require('../Config');
var Utils = require('../services/Utils');
var Graph = require('node-dijkstra');

var synchronousShopping = {
     checkInputData: function(data) {
        var res = new Object(),
                inputData = data.input,
                configInitParams = [],
                totalShoppingCenters = 0,
                totalRoads = 0,
                totalFishes = 0;

        res.valid = false;
        //check if inputData exists
        if (!inputData) {
            res.reason = Config.ERROR_MSG.TYPE_1;
            return res;
        }
        //check if inputData length is consistent
        if (inputData.length < Config.MIN_CONFIG_LINES) {
            res.reason = Config.ERROR_MSG.TYPE_2;
            return res;
        }
        configInitParams = inputData[0];
        //check if first line length is consistent
        if (configInitParams.length !== Config.INIT_PARAMS) {
            res.reason = Config.ERROR_MSG.TYPE_3;
            return res;
        }
        //set numbers of shoppingCenters, roads and fishes
        totalShoppingCenters = configInitParams[0];
        totalRoads = configInitParams[1];
        totalFishes = configInitParams[2];

        //check if Shoppingenter/Roads/Fish are between limits
        if (!Utils.checkLimits(totalShoppingCenters, totalRoads, totalFishes)) {
            res.reason = Config.ERROR_MSG.TYPE_4;
            return res;
        }

        //check if number of shopping center and roads is consistent 
        if ((inputData.length - Config.MIN_CONFIG_LINES) !== (totalShoppingCenters + totalRoads)) {
            res.reason = Config.ERROR_MSG.TYPE_5;
            return res;
        }
        //check shoppingcenters with correct fish data
        if (!Utils.checkShoppingCentersData(inputData, totalShoppingCenters, totalFishes)) {
            res.reason = Config.ERROR_MSG.TYPE_6;
            return res;
        }
        //check fish sold by shopping center / check if all fish are sold by at least one fishmonger
        if (!Utils.checkFishSoldByShoppingCenter(inputData, totalShoppingCenters, totalFishes)) {
            res.reason = Config.ERROR_MSG.TYPE_7;
            return res;
        }

        //check roads data
        if (!Utils.checkRoadsData(inputData, totalShoppingCenters, totalRoads)) {
            res.reason = Config.ERROR_MSG.TYPE_8;
            return res;
        }
        //check repeated roads
        if (!Utils.checkRepeatedRoads(inputData, totalShoppingCenters, totalRoads)) {
            res.reason = Config.ERROR_MSG.TYPE_9;
            return res;
        }
        res.valid = true;
        return res;
    },
    buildInputData: function(inputData, totalShoppingCenters, totalRoads) {
        var shoppingCenterList = [];

        for (var i = 1; i <= totalShoppingCenters; i++) {
            var fishes = inputData[i];
            var directShoppingCenterList = [];
            var firstRoadIndex = (totalShoppingCenters + Config.MIN_CONFIG_LINES);
            var lastRoadIndex = firstRoadIndex + (totalRoads - 1);
            //remove the number of type of fish sold by the shopping center
            fishes.shift();
            //Build list of direct shopping centers
            directShoppingCenterList = synchronousShopping.buildRoadData(inputData, i, firstRoadIndex, lastRoadIndex)
            shoppingCenterList.push(new ShoppingCenter(i, fishes, directShoppingCenterList))
        }
        return shoppingCenterList;
    },
    buildRoadData: function(inputData, shoppingCenterNumber, fromRoad, toRoad) {
        var shoppingCenterList = [];
        for (var j = fromRoad; j <= toRoad; j++) {
            
            var road = inputData[j];
            var directShoppingCenter = {};
            var fromShoppingCenter = road[0];
            var toShoppingCenter = road[1];
            var timeBetweenShoppingCenters = road[2];
            //add the direct shopping center
            if (fromShoppingCenter === shoppingCenterNumber) {
                directShoppingCenter.to = toShoppingCenter;
            }
            if (toShoppingCenter === shoppingCenterNumber) {
                directShoppingCenter.to = fromShoppingCenter;
            }
            if (directShoppingCenter.to) {
                directShoppingCenter.time = timeBetweenShoppingCenters;
                shoppingCenterList.push(directShoppingCenter);
            }
        }
        return shoppingCenterList;
    },
    checkAllShoppingCenterRoutes: function(visitedShoppingCenters, fromShoppingCenter, shoppingCenterList) {
        var nextVisitedShoppingCenters = [];
        var lastShoppingCenterVisitedIndex = visitedShoppingCenters.length - 1;
        var currentShoppingCenter = visitedShoppingCenters[lastShoppingCenterVisitedIndex];
        var currentShoppingCenterIndex = currentShoppingCenter - 1;
        //lets't go to every direct shopping center
        _.each(shoppingCenterList[currentShoppingCenterIndex].getShoppingCenters(), function(directShoppingCenterData) {
            var directShoppingCenter = directShoppingCenterData.to;
            //Check if the direct shopping center is not the previous shopping center or it has not visited yet
            if(_.indexOf(visitedShoppingCenters, directShoppingCenter) < 0 && directShoppingCenter != fromShoppingCenter){
                visitedShoppingCenters.push(directShoppingCenter);
                nextVisitedShoppingCenters = synchronousShopping.checkAllShoppingCenterRoutes(visitedShoppingCenters, directShoppingCenter, shoppingCenterList);
            }
        });
        //return all shopping center visited
        return _.uniq(_.union(visitedShoppingCenters, nextVisitedShoppingCenters));
    },
    
    
    findShoppingCenterRoutes: function(shoppingRouteList, shoppingCenterList) {
        var newShoppingRouteList = []; 
        var finish = true;
        var nextShoppingCenters = [];
        _.each(shoppingRouteList, function(shoppingRoute) {
            for (var i = 1; i < shoppingCenterList.length; i++) {
                //Discard route if shoppingCenter as been already added or current shopping Center has not fish
                if (_.indexOf(shoppingRoute.getRoute(), i) < 0 && shoppingCenterList[i - 1].getFishes().length > 0) {
                    finish = false;
                    //add fish collected by each shopping center
                    var fishes = _.uniq(_.union(shoppingRoute.getFishes(), shoppingCenterList[i - 1].getFishes()));
                    //add the current shopping center to the route
                    var newRoute = _.union(shoppingRoute.getRoute(), [i]);
                    //Instance the the route with its respective fish 
                    var newShoppingRoute = new ShoppingRoute(newRoute, fishes);
                    newShoppingRouteList.push(newShoppingRoute);
                }
            }
        });
        // check if there are shopping centers pending for add to the route
        if (!finish)
            nextShoppingCenters = synchronousShopping.findShoppingCenterRoutes(newShoppingRouteList, shoppingCenterList);
        return _.union(newShoppingRouteList, nextShoppingCenters);
    },
    chooseBestTwoRoutes: function(ShoppingRouteList, totalFishes, maxTime) {
        var bestTwoRoutes = {};
        for (var i = 0; i < (ShoppingRouteList.length); i++) {
            for (var j = i; j < ShoppingRouteList.length; j++) {
                var firstRoute = ShoppingRouteList[i];
                var secondRoute = ShoppingRouteList[j];
                //add fish of both routes
                var fishesCollected = _.uniq(_.union(firstRoute.getFishes(), secondRoute.getFishes()));
                //check if each two combinations of route contains all fish
                //if one of the routes has not fish then they go together by the route with fish
                if (fishesCollected.length === totalFishes && firstRoute.getFishes().length > 0 && secondRoute.getFishes().length > 0) {
                    var maxTimeBetweenPaths = 0;
                    //get the max time between two routes
                    if (firstRoute.totalTime >= secondRoute.totalTime){
                        maxTimeBetweenPaths = firstRoute.totalTime;
                    }
                    else{
                        maxTimeBetweenPaths = secondRoute.totalTime;
                    }
                    //check if the max time between routes is the best
                    if (maxTimeBetweenPaths <= maxTime) {
                        maxTime = maxTimeBetweenPaths;
                    }
                }
            }
        }
        bestTwoRoutes.maxTime = maxTime;
        return bestTwoRoutes;
    }
};
module.exports = synchronousShopping;