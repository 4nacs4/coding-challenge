// Constructor
function ShoppingCenter(id, fishes, shoppingCenters) {
    // always initialize all instance properties
    this.id = id;
    this.fishes = fishes;
    this.shoppingCenters = shoppingCenters;
}
// class methods
ShoppingCenter.prototype.getId = function() {
    return this.id;
};
ShoppingCenter.prototype.getFishes = function() {
    return this.fishes;
};
ShoppingCenter.prototype.getShoppingCenters = function() {
    return this.shoppingCenters;
};
// export the class
module.exports = ShoppingCenter;