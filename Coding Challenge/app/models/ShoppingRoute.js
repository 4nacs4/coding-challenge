var _ = require('underscore');
// Constructor
function ShoppingRoute(route, fishes) {
    // always initialize all instance properties
    this.route = route;
    this.fishes = fishes;
}
// class methods
ShoppingRoute.prototype.getRoute = function() {
    return this.route;
};
ShoppingRoute.prototype.getFishes = function() {
    return this.fishes;
};
ShoppingRoute.prototype.getTotalTimeRoute = function(shortRoutes) {
    
    var totalTime = 0;
    //get the total time at visit all shopping center's route
    for (var i = 0; i < (this.route.length - 1); i++) {
        var fromShoppingCenter = this.route[i], toShoppingCenter = this.route[i+1];
        var routeBetweenShoppingCenters = shortRoutes.path(String(fromShoppingCenter), String(toShoppingCenter), {cost: true});
        totalTime = totalTime + routeBetweenShoppingCenters.cost;
    }
    this.totalTime = totalTime;
};
ShoppingRoute.prototype.addLastShoppingCenterToBeVisited = function(shoppingCenterList) {
    //get the shopping center N
    var lastShoppingCenter = shoppingCenterList[(shoppingCenterList.length - 1)];
    //add the last shopping center with respective fishes
    this.route.push(shoppingCenterList.length);
    this.fishes = _.uniq(_.union(this.fishes, lastShoppingCenter.fishes));
};
// export the class
module.exports = ShoppingRoute;