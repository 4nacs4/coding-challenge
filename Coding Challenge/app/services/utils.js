var _ = require('underscore');
var Config = require('../Config');
var Graph = require('node-dijkstra');

var utils = {
    checkShoppingCentersData: function(shoppingCenterData, totalShoppingCenters, totalFishes) {
        var res = true;
        for (var i = Config.MIN_CONFIG_LINES; i <= totalShoppingCenters; i++) {
            var shoppingCenter = shoppingCenterData[i];
            //check shopping center data
            if (!utils.checkSingleShoppingCenterData(shoppingCenter, totalFishes)) {
                res = false;
                break;
            }
        }
        return res;
    },
    checkSingleShoppingCenterData: function(shoppingCenter, totalFishes) {
        var res = false;
        var fishesSoldByShoppingCenter = shoppingCenter[0];
        //check if shopping center is not empty
        if (shoppingCenter.length > 0) {
            //check if fish sold are between limits
            if (fishesSoldByShoppingCenter >= Config.MIN_TYPE_FISH_BY_FISHMONGER && fishesSoldByShoppingCenter <= totalFishes) {
                //check if data is consistent
                var totalFishTypes = (shoppingCenter.length - 1);
                if (totalFishTypes === fishesSoldByShoppingCenter) {
                    //check if fish are between limits
                    if (fishesSoldByShoppingCenter > 0) {
                        for (var j = 1; j <= fishesSoldByShoppingCenter; j++) {
                            var fishTypeSold = shoppingCenter[j];
                            if ((fishTypeSold >= Config.MIN_TYPE_FISH && fishTypeSold <= totalFishes)) {
                                res = true;
                            }
                            else {
                                res = false;
                                break;
                            }
                        }
                    }
                    else {
                        res = true;
                    }
                }
            }
        }
        return res;
    },
    checkFishSoldByShoppingCenter: function(shoppingCenterData, totalShoppingCenters, totalFishes) {
        var res = true;
        var totalFishesSoldBitville = [];
        for (var i = Config.MIN_CONFIG_LINES; i <= totalShoppingCenters; i++) {
            var shoppingCenter = shoppingCenterData[i];
            var totalFishSoldByShoppingCenter = [];
            var fishNumberSold = shoppingCenter[0];
            //add fish by shopping center
            for (var j = 1; j <= fishNumberSold; j++) {
                var fishTypeSold = shoppingCenter[j];
                totalFishSoldByShoppingCenter.push(fishTypeSold);
            }
            totalFishSoldByShoppingCenter = _.uniq(totalFishSoldByShoppingCenter);
            //check if fish are not repeated by shopping center
            if (fishNumberSold !== totalFishSoldByShoppingCenter.length) {
                res = false;
                break;
            }
            totalFishesSoldBitville = _.uniq(_.union(totalFishesSoldBitville, totalFishSoldByShoppingCenter));
        }
        //check if all fish is sold
        if (totalFishesSoldBitville.length !== totalFishes) {
            res = false;
        }
        return res;
    },
    checkRoadsData: function(inputData, totalShoppingCenters, totalRoads) {
        var res = true;
        var firstRoadIndex = (totalShoppingCenters + Config.MIN_CONFIG_LINES1);
        var lastRoadIndex = firstRoadIndex + (totalRoads - 1);

        for (var i = firstRoadIndex; i <= lastRoadIndex; i++) {
            var road = inputData[i];
            //check road  data
            if (!utils.checkSingleRoadData(road, totalShoppingCenters)) {
                res = false;
                break;
            }
        }
        return res;
    },
    checkSingleRoadData: function(roadData, totalShoppingCenters) {
        var res = false;
        //check if data is consistent
        if (roadData.length === Config.ROADS_PARAMS) {
            var fromShoppingCenter = roadData[0];
            var toShoppingCenter = roadData[1];
            var time = roadData[2];
            //check limits
            if ((fromShoppingCenter >= 1 && fromShoppingCenter <= totalShoppingCenters) && (toShoppingCenter >= 1 && toShoppingCenter <= totalShoppingCenters)) {
                //check if road connects a shopping center to itself
                if (fromShoppingCenter != toShoppingCenter) {
                    if (time >= Config.MIN_TIME && time <= Config.MAX_TIME) {
                        res = true;
                    }
                }
            }
        }
        return res;
    },
    checkRepeatedRoads: function(inputData, totalShoppingCenters, totalRoads) {
        var res = true;
        var pairOfShoppingCenterList = [];
        var firstRoadIndex = (totalShoppingCenters + Config.MIN_CONFIG_LINES);
        var lastRoadIndex = firstRoadIndex + (totalRoads - 1);

        for (var i = firstRoadIndex; i <= lastRoadIndex; i++) {
            var road = inputData[i];
            var fromShoppingCenter = road[0];
            var toShoppingCenter = road[1];
            var pairOfShoppingCenter = "";
            //check if shopping center are not in different order
            if (fromShoppingCenter < toShoppingCenter)
                pairOfShoppingCenter = String(fromShoppingCenter) + "," + String(toShoppingCenter);
            else
                pairOfShoppingCenter = String(toShoppingCenter) + "," + String(fromShoppingCenter);

            pairOfShoppingCenterList.push(pairOfShoppingCenter);
        }
        //check if only one road connects one pair of shopping center
        if (pairOfShoppingCenterList.length != (_.uniq(pairOfShoppingCenterList)).length) {
            res = false;
        }
        return res;
    },
    checkLimits: function(totalShoppingCenters, totalRoads, totalFishes) {
        var res = false;
        if (totalShoppingCenters >= Config.MIN_SHOPPING_CENTERS && totalShoppingCenters <= Config.MAX_SHOPPING_CENTERS) {
            if (totalRoads >= Config.MIN_ROADS && totalRoads <= Config.MAX_ROADS) {
                if (totalFishes >= Config.MIN_TYPE_FISH && totalFishes <= Config.MAX_TYPE_FISH) {
                    res = true;
                }
            }
        }
        return res;
    },
    buildDijkstraGraph: function(shoppingCenterList) {
        var shortRoutes = new Graph();
        _.each(shoppingCenterList, function(shoppingCenter) {
            var directShoppingCenter = {};
            //create a list with all direct shopping centers with respective time
            _.each(shoppingCenter.getShoppingCenters(), function(shoppingCenterData) {
                directShoppingCenter[String(shoppingCenterData.to)] = shoppingCenterData.time;
            });
            //create nodes by each shopping center
            shortRoutes.addNode(String(shoppingCenter.getId()), directShoppingCenter);
        });
        return shortRoutes;
    },
};


module.exports = utils;