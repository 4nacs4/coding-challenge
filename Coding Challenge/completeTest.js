var express = require("express");
var app = express();
var config = require('./app/config');
var SynchronousShopping = require('./app/controllers/synchronousShopping');
var Utils = require('./app/services/utils');
var ShoppingRoute = require('./app/models/ShoppingRoute');
var input1 = require('./tests/input1.json');

app.listen(config.PORT, function() {
    console.log("Node test server running on http://localhost:3000");

    console.log("----------TESTING FULL DATA----------")
    var complete = false;
    var step1 = (SynchronousShopping.checkInputData(input1));
    if (step1.valid) {
        console.log("----------TESTING INPUT DATA PASSED---------- ");

        var step2 = (SynchronousShopping.buildInputData(input1.input, input1.input[0][0], input1.input[0][1]));
        if (step2.length == input1.input[0][0]) {
            console.log("----------TESTING BUILD DATA PASSED---------- ");

            var step3 = (SynchronousShopping.checkAllShoppingCenterRoutes([1], 1, step2));
            if (step3.length == input1.input[0][0]) {
                console.log("----------TESTING CHECK ROUTES PASSED---------- ");
                var step4 = Utils.buildDijkstraGraph(step2);
                if (step4.graph) {
                    console.log("----------TESTING BUILD GRAPH PASSED---------- ");

                    var firstRoute = new ShoppingRoute([1], [1]);
                    var step5 = SynchronousShopping.findShoppingCenterRoutes([firstRoute], step2);
                    if (step5.length > 0) {
                        console.log("----------TESTING POSIBLE ROUTES PASSED---------- ");

                    var step6 = (SynchronousShopping.chooseBestTwoRoutes(step5, input1.input[0][2], 9999999999999));

                        if (step6.maxTime) {
                            complete = true;
                             console.log("----------TESTING TWO BEST ROUTES PASSED---------- ");
                        }
                        else {
                             console.log("----------TESTING TWO BEST ROUTES PASSED---------- ");
                        }
                    }
                    else {
                        console.log("----------TESTING POSIBLE ROUTES ERROR---------- ");
                    }
                }
                else {
                    console.log("----------TESTING BUILD GRAPH DATA ERROR---------- ");
                }

            }
            else {
                console.log("----------TESTING CHECK ROUTES DATA ERROR---------- ");
            }
        }
        else {
            console.log("----------TESTING BUILD DATA ERROR---------- ");
        }


    }
    if(complete)
         console.log("----------COMPLETE TEST PROCCESS PASSED---------- ");
     else
          console.log("----------COMPLETE TEST PROCCESS FAIL---------- ");

});


