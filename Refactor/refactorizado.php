<?	
	$error = array(
		'no_error' => '0',
		'type_2' => '1',
		'type_3' => '3',
		'type_4' => '4',
		'type_5' => '5',
	);
	$flags = array(
		'invalid_service_id' => '6',
		'valid_service_id' => '1',
		'next_service_status_id' => '2',
		'driver_available_status' => '0',
		'iphone_type_user' => '1',
		'iphone_tone' => 'honk.wav',
		'android_tone' => 'default'
	);
	public function postConfirm(){
		$pushMessage = 'Tu servicio ha sido confirmado';
		$res = array('error' => $error->type_3);

		if(!(Input::has('service_id') && Input::has('driver_id')){
			$res->error = $error->type_4;
			return Response::json($res);	
		}
		if((Input::get('service_id') == NULL || (Input::get('driver_id') == NULL){
			$res->error = $error->type_4;
			return Response::json($res);		
		}

		$serviceId = Input::get('service_id');
		$driverId = Input::get('driver_id');

		$service = Service::find($serviceId);

		if($service == NULL){
			return Response::json($res);
		}
		if($service->status_id == NULL){
			return Response::json($res);
		}
		if ($service->status_id == $flags->invalid_service_id) {
			return Response::json($res) 
		}
		if (!($service->driver_id == NULL && ($service->status_id == $flags->valid_service_id)) {
			$res->error = $error->type_2;
			return Response::json($res);
		}

		$driver = Driver::find($driverId);
		if($driver == NULL){
			return Response::json($res);
		}
		if($driver->car_id == NULL){
			return Response::json($res);
		}
		$serviceUpdated = Service::update($serviceId, array(
			'driver_id' => $driverId,
			'status_id' => $flags->next_service_status_id,
			'car_id' => $driver->car_id
		));
		if($serviceUpdated == NULL){
			$res->error = $error->type_5;
			return Response::json($res);	
		}
		$driverUpdated = Driver::update($driverId, array(
			'available' => $flags->driver_available_status;
		));
		if($driverUpdated == NULL){
			$res->error = $error->type_5;
			return Response::json($res);	
		}

		//Notificar al usuario!!
		notifyUser($service, $pushMessage);
		$res->error = $error->no_error;
		return Response::json($res);	
	}
	public function notifyUser($service, $message){
		$param1 = '1';
		$param2 = 'Open';
		$serviceId = array('service_id' => $service->id);
		$push = Push::make();
		if($service->user->uuid != ''){
			if($service->user->type == $flags->iphone_type_user){ //iPhone
				$result = $push->ios($service->user->uuid, $message, $param1, $flags->iphone_tone, $param2, $serviceId);
			}
			else{
				$result = $push->android2($service->user->uuid, $message, $param1, $flags->android_tone, $param2,  $serviceId);
			}
		}
	}
?>